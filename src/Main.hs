module Main where

import Choreography
import Data.Proxy
import Data.Time
import GHC.TypeLits
import System.Environment
import qualified Data.ByteString as BIO
import qualified Data.ByteString as B
import qualified Data.Text as T
import qualified Data.Word as W
import qualified Data.Foldable as F
import qualified Data.Ord as O
import qualified Data.Maybe as My
import qualified Data.Hashable as H
import qualified Data.Bits as B
import GHC.Integer (eqInteger)


syncFrom :: Proxy "syncFrom"
syncFrom = Proxy

syncTo :: Proxy "syncTo"
syncTo = Proxy



-- NOTE: there's got to be something faster than + here
toWord32 :: W.Word16 -> W.Word16-> W.Word32
toWord32 w0 w1 = (fromIntegral w1) + (B.shiftL 16 . fromIntegral $ w1)

-- rotatingHash :: Word -> B.ByteString -> [W.Word16]
-- rotatingHash chunkSize b = scanl nextHash 0 $ zip ((replicate (fromIntegral chunkSize) 0) ++ (drop (fromIntegral chunkSize) word16s)) word16s
--   where
--     chunks :: [W.Word16]
--     chunks = B.zipWith toWord16 b (B.tail b)


-- the non-rolling versions, purely for testing
rsyncA :: B.ByteString -> W.Word16
rsyncA = fromIntegral . modM . B.foldl (\n -> (+) n . fromIntegral) 0

rsyncB :: B.ByteString -> W.Word16
rsyncB = modM . sum . zipWith (*) [1..] . map fromIntegral . B.unpack


rsyncA' :: W.Word16 -> (W.Word8, W.Word8) -> W.Word16
rsyncA' prevHash (removeByte, addByte) =  modM $ (fromIntegral prevHash - (fromIntegral removeByte)) + (fromIntegral addByte)

rsyncB' :: W.Word32 -> W.Word32 -> W.Word32 -> W.Word32 -> W.Word16
rsyncB' prevB chunkSize newA removeByte = modM $ prevB - ((succ chunkSize) * removeByte) + newA


-- highest prime below 2^16
modM :: Integral a => W.Word32 -> a
modM = fromIntegral . flip mod 65521


    -- turns out Words overflow normally in Haskell, making this part easy
    -- nextHash :: W.Word16 -> (W.Word16, W.Word16) -> W.Word16
    -- nextHash prevHash (valToRemove, valToAdd) = (prevHash - valToRemove) + (fromIntegral valToAdd)

    -- modByHashSize :: W.Word32 -> W.Word16
    -- modByHashSize = fromIntegral . flip mod (fromIntegral . maxBound $ (0 :: W.Word16))

numBytesPerHash :: Int
numBytesPerHash = 8

hashes :: B.ByteString -> [(Int, B.ByteString)]
hashes "" = []
hashes bytes = (H.hash curr, curr) : hashes rest
  where
    (curr, rest) = B.splitAt numBytesPerHash bytes

sndIfNotEq :: Eq a => (a, b) -> a -> Maybe b
sndIfNotEq (fstHash, actual) sndHash = if sndHash == fstHash then Nothing else Just actual

sync :: T.Text -> Choreo IO (() @ "syncTo")
sync filePath = do
  toHashesAll <- locally syncTo . const $ hashes <$> BIO.readFile (T.unpack filePath)
  toHashesJustHashes <- locally syncTo $ \un -> pure . map fst . un $ toHashesAll
  toHashesJustHashes' <- (syncTo, toHashesJustHashes) ~> syncFrom
  diffs <- locally syncFrom $ \un -> do
    fromHashes <- hashes <$> BIO.readFile (T.unpack filePath)
    pure $ zipWith sndIfNotEq fromHashes (un toHashesJustHashes')
  diffs' <- (syncFrom, diffs) ~> syncTo
  locally syncTo $ \un -> do
    let bytestrings :: [B.ByteString] = zipWith My.fromMaybe (map snd . un $ toHashesAll) (un diffs')
    BIO.writeFile (T.unpack filePath) . B.concat $ bytestrings

main :: IO ()
main = do
  [loc] <- getArgs
  case loc of
    "syncFrom"  -> runChoreography cfg choreo "syncFrom"
    "syncTo" -> runChoreography cfg choreo "syncTo"
  return ()
  where
    choreo = sync "Main.hs"

    cfg = mkHttpConfig [ ("syncFrom",  ("localhost", 4242))
                       , ("syncTo", ("localhost", 4343))
                       ]
