# Rsync (not really), implemented using [HasChor](https://github.com/gshen42/HasChor/tree/main)

This is mostly a learning exercise for HasChor, a [paper](https://github.com/gshen42/HasChor/tree/main) & [library](https://github.com/gshen42/HasChor/tree/main) for choreographic programming in Haskell. It skips rsync's clever rotating hash & just hashes chunks, compares hashes, and transfers the difference.

# Build
with cabal

``` sh
mv cabal-only-cabal.project.local cabal.project.local

cabal run haschor-rsync -- syncFrom # on from file
cabal run haschor-rsync -- syncTo # on the other file
```

with nix
``` sh
# to build
nix build
# to run
./result syncFrom # in one file
./result syncTo # in another file
```
